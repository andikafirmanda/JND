<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Admin</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu SIAP', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard','icon' => 'fa fa-dashboard', 'url' => ['/site/index'], 'visible'=>(!Yii::$app->user->isGuest)],
    
                    [
                        'label' => 'Data Master',
                        'url' => '#', 
                        'items' => [
                            ['label' => 'Kelola Data Barang', 'url' => 'http://localhost/tugasbesar_web2/web/index.php?r=databarang/index',],
                            ['label' => 'Kelola Data Supplier', 'url' => ['datasupplier/index'],],
                        ], 'visible'=>(!Yii::$app->user->isGuest),
                    ],
               
                    
                    [
                        'label' => 'Transaksi',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Barang Masuk', 'url' => ['barangmasuk/index'], ],
                            ['label' => 'Barang Keluar', 'url' => ['barangkeluar/index'],],
                        ], 'visible'=>(!Yii::$app->user->isGuest),
                    ], 
                    [
                        'label' => 'Laporan',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Data Barang', 'url' => ['databarang/cetak'],],
                            ['label' => 'Barang Masuk', 'url' => ['barangmasuk/cetak'],],
                            ['label' => 'Barang Keluar', 'url' => ['barangkeluar/cetak'],],
                        ], 'visible'=>(!Yii::$app->user->isGuest),
                    ],
    
                    [
                        'label' => 'Pengaturan',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Tambah Menu (GII)', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                        ], 'visible'=>(!Yii::$app->user->isGuest),
                    ],  
                    
                    
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ], 
            ]
        ) ?>

    </section>

</aside>
