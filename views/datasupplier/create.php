<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataSupplier */

$this->title = 'Create Data Supplier';
$this->params['breadcrumbs'][] = ['label' => 'Data Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-supplier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
