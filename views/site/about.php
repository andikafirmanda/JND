<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Aplikasi ini dibuat oleh : <br>
        - Adi Widia Putra <br>
        - Andri Budi Santoso <br>
        - Mochammad Ardi Miradi <br>
    </p>

   
</div>
