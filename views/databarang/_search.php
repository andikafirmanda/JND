<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataBarangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-barang-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_databarang') ?>

    <?= $form->field($model, 'nama_sparepart') ?>

    <?= $form->field($model, 'jenis_sparepart') ?>

    <?= $form->field($model, 'stok_sparepart') ?>

    <?= $form->field($model, 'harga_sparepart') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
