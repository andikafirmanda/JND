<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataBarang */

$this->title = 'Update Data Barang: ' . $model->id_databarang;
$this->params['breadcrumbs'][] = ['label' => 'Data Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_databarang, 'url' => ['view', 'id' => $model->id_databarang]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="data-barang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
