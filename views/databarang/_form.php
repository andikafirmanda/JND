<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataBarang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-barang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_sparepart')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_sparepart')->dropDownList([ 'Knalpot' => 'knalpot', 'rem' => 'rem', 'Suspensi' => 'Suspensi', 'kemudi' => 'kemudi', 'roda' => 'roda', 'gear' => 'gear'],  ['prompt' => '--pilih--']) ?>

    <?= $form->field($model, 'stok_sparepart')->textInput() ?>

    <?= $form->field($model, 'harga_sparepart')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
