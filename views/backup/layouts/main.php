<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'SIPS',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Kelola Data Barang', 'url' => ['/databarang/index']],
            [
            'label' => 'Transaksi',
            'items' => [
                 ['label' => 'Barang Masuk', 'url' => 'http://localhost/tugasbesar_web2/web/index.php?r=barangmasuk/index'],
                 ['label' => 'Barang Keluar', 'url' => 'http://localhost/tugasbesar_web2/web/index.php?r=barangkeluar/index'],
            ],
            ],
            [
            'label' => 'Laporan',
            'items' => [
                 ['label' => 'Laporan Data Barang', 'url' => '#'],
                 ['label' => 'Laporan Barang Masuk', 'url' => '#'],
                 ['label' => 'Laporan Barang Keluar', 'url' => '#'],
            ],
            ],
            [
            'label' => 'Help',
            'items' => [
                 ['label' => 'Tentang', 'url' => ['/site/about']],
                 ['label' => 'Kontak', 'url' => ['/site/contact']],
            ],
            ],
            
            
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
