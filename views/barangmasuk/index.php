<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BarangMasukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Barang Masuk';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barang-masuk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Barang Masuk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_barangmasuk',
            //'id_supplier',
            //'id_databarang',
            //nama supplier
            [
                'attribute' => 'nama_supplier',
                'value' => function($data) {
                    return $data->dataSuppliers->nama_supplier;
                }
            ],
            //nama barang
            [
                'attribute' => 'nama_barang',
                'value' => function($data) {
                    return $data->dataBarangs->nama_sparepart;
                }
            ],
            'tanggal_masuk',
            'stok_masuk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
