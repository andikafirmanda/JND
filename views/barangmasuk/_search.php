<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BarangMasukSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barang-masuk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_barangmasuk') ?>

    <?= $form->field($model, 'id_supplier') ?>

    <?= $form->field($model, 'id_databarang') ?>

    <?= $form->field($model, 'tanggal_masuk') ?>

    <?= $form->field($model, 'stok_masuk') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
