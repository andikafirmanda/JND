<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BarangKeluarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Barang Keluar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barang-keluar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Barang Keluar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_barangkeluar',
            //'id_databarang',
            [
                'attribute' => 'nama_barang',
                'value' => function($data) {
                    return $data->dataBarangs->nama_sparepart;
                }
            ],
            'tanggal_keluar',
            'stok_keluar',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
