<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\DataBarang; 


/* @var $this yii\web\View */
/* @var $model app\models\BarangKeluar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barang-keluar-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?=
        $form->field($model, 'id_databarang')->dropDownList(
            ArrayHelper::map(DataBarang::find()->all(),'id_databarang','nama_sparepart'),
        ['prompt'=>'Pilih Sparepart']
        )
    ?>

    <?= $form->field($model, 'stok_keluar')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
