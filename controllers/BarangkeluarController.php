<?php

namespace app\controllers;

use Yii;
use app\models\BarangKeluar;
use app\models\DataBarang;
use app\models\BarangKeluarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BarangkeluarController implements the CRUD actions for BarangKeluar model.
 */
class BarangkeluarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BarangKeluar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BarangKeluarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BarangKeluar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BarangKeluar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   
    
    /*public function actionCreate()
    {
        $model = new BarangKeluar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_barangkeluar]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    */
    
    public function actionCreate()
    {
        $model = new BarangKeluar();

        if ($model->load(Yii::$app->request->post())) {
            
            $validasi = DataBarang::findOne($model['id_databarang']);
            
            if ($model->stok_keluar > $validasi->stok_sparepart)
            {
                return $this->render('kurang');
               
            } 
            
            else if ($model->stok_keluar < 1)
            {
                return $this->render('tidak');
            }
            
            else {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id_barangkeluar]);
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    //revisi
    /*
     public function actionValidasi()
    {
        $model = new BarangKeluar();
        $sisa_stok = $model->totalStock() - $model->

        if ($model->load(Yii::$app->request->post()) && $model->totalStock()) {
            return $this->redirect(['view', 'id' => $model->id_barangkeluar]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /*
    
    /**
     * Updates an existing BarangKeluar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_barangkeluar]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BarangKeluar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BarangKeluar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BarangKeluar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BarangKeluar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCetak()
    {
        return $this->render('cetak');
    }
    
     public function actionKurang()
    {
        return $this->render('kurang');
    }
    
    public function actionTidak()
    {
        return $this->render('tidak');
    }
}
