<?php

use yii\db\Schema;
use yii\db\Migration;

class m170109_132245_create_table_data_supplier extends Migration
{
    public function up()
    {
         $this->createTable('data_supplier', [
            'id_supplier' => Schema::TYPE_PK,
            'nama_supplier' => Schema::TYPE_STRING . ' NOT NULL',
            'alamat_supplier' => Schema::TYPE_STRING . ' NOT NULL',
            'kontak_supplier' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
    }

    public function down()
    {
          $this->dropTable('data_supplier');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
