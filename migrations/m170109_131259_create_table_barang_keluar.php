<?php

use yii\db\Schema;
use yii\db\Migration;

class m170109_131259_create_table_barang_keluar extends Migration
{
        public function up()
    {
         $this->createTable('barang_keluar', [
            'id_barangkeluar' => Schema::TYPE_PK,
            'id_databarang' => Schema::TYPE_INTEGER . ' NOT NULL',
            'tanggal_keluar' => Schema::TYPE_DATETIME . ' DEFAULT CURRENT_TIMESTAMP NOT NULL',
            'stok_keluar' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        
        $this->addForeignKey(
        'fk_fk_barang_keluar', 
        'barang_keluar', 'id_databarang', 
        'data_barang', 'id_databarang', 
        'restrict', 'cascade');
    }


    public function down()
    {
        $this->dropTable('barang_keluar');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
