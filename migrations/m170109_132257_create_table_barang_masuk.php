<?php

use yii\db\Schema;
use yii\db\Migration;

class m170109_132257_create_table_barang_masuk extends Migration
{
    public function up()
    {
        $this->createTable('barang_masuk', [
            'id_barangmasuk' => Schema::TYPE_PK,
            'id_supplier' => Schema::TYPE_INTEGER . ' NOT NULL',
            'id_databarang' => Schema::TYPE_INTEGER . ' NOT NULL',
            'tanggal_masuk' => Schema::TYPE_DATETIME . ' DEFAULT CURRENT_TIMESTAMP NOT NULL',
            'stok_masuk' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        
        $this->addForeignKey(
        'fk_fk_barang_masuk', 
        'barang_masuk', 'id_databarang', 
        'data_barang', 'id_databarang', 
        'restrict', 'cascade');
        
        $this->addForeignKey(
        'fk_fk_supplier_masuk', 
        'barang_masuk', 'id_supplier', 
        'data_supplier', 'id_supplier', 
        'restrict', 'cascade');

    }

    public function down()
    {
        $this->dropTable('barang_masuk');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
