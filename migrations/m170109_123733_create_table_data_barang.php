<?php

use yii\db\Schema;
use yii\db\Migration;

class m170109_123733_create_table_data_barang extends Migration
{
    public function up()
    {
         $this->createTable('data_barang', [
            'id_databarang' => Schema::TYPE_PK,
            'nama_sparepart' => Schema::TYPE_STRING . ' NOT NULL',
            'jenis_sparepart' => Schema::TYPE_STRING . ' NOT NULL',
            'stok_sparepart' => Schema::TYPE_INTEGER . ' NOT NULL',
            'harga_sparepart' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('data_barang');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
