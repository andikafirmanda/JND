<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "barang_masuk".
 *
 * @property integer $id_barangmasuk
 * @property integer $id_supplier
 * @property integer $id_databarang
 * @property string $tanggal_masuk
 * @property integer $stok_masuk
 */
class BarangMasuk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'barang_masuk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_supplier', 'id_databarang', 'stok_masuk'], 'required'],
            [['id_supplier', 'id_databarang', 'stok_masuk'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_barangmasuk' => 'Id Barangmasuk',
            'id_supplier' => 'Id Supplier',
            'id_databarang' => 'Id Databarang',
            'stok_masuk' => 'Stok Masuk',
        ];
    }
    
    //relasi 
    public function getDataBarangs()
    {
        return $this->hasOne(DataBarang::className(), ['id_databarang' => 'id_databarang']);
    }  
    
    public function getDataSuppliers()
    {
        return $this->hasOne(DataSupplier::className(), ['id_supplier' => 'id_supplier']);
    }  
}
