<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "barang_keluar".
 *
 * @property integer $id_barangkeluar
 * @property integer $id_databarang
 * @property string $tanggal_keluar
 * @property integer $stok_keluar
 */
class BarangKeluar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'barang_keluar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_databarang', 'stok_keluar'], 'required'],
            [['id_databarang', 'stok_keluar'], 'integer'],
           // ['id_databarang', 'checkmaxandminvalues'],
        ];
    }

    
   /* public function checkmaxandminvalues($attribute, $params)
    {
        $your_product=DataBarang::find()->where(['id_databarang'=>$this->id_databarang])->one();

        //Here you do your validation logic
        if($your_product->id_databarang===20 && $your_product->stok_sparepart < 0)
           {

                $this->addError('stok_keluar', 'Produk kurang dari 0');
           }
    } 
    */
    
    /*
    public function totalStock() {
        $validasi_stok=DataBarang::find()->where(['id_databarang'=>$this->id_databarang])->one();
        return $validasi_stok->stok_keluar;
    }
    /*
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_barangkeluar' => 'Id Barangkeluar',
            'id_databarang' => 'Id Databarang',
            'stok_keluar' => 'Stok Keluar',
        ];
    }
    
    //relasi 
    public function getDataBarangs()
    {
        return $this->hasOne(DataBarang::className(), ['id_databarang' => 'id_databarang']);
    }  
}
