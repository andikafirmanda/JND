<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_supplier".
 *
 * @property integer $id_supplier
 * @property string $nama_supplier
 * @property string $alamat_supplier
 * @property string $kontak_supplier
 */
class DataSupplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_supplier', 'alamat_supplier', 'kontak_supplier'], 'required'],
            [['nama_supplier', 'alamat_supplier'], 'string', 'max' => 50],
            [['kontak_supplier'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_supplier' => 'Id Supplier',
            'nama_supplier' => 'Nama Supplier',
            'alamat_supplier' => 'Alamat Supplier',
            'kontak_supplier' => 'Kontak Supplier',
        ];
    }
}
