<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_barang".
 *
 * @property integer $id_databarang
 * @property string $nama_sparepart
 * @property string $jenis_sparepart
 * @property integer $stok_sparepart
 * @property integer $harga_sparepart
 */
class DataBarang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_barang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_sparepart', 'jenis_sparepart', 'stok_sparepart', 'harga_sparepart'], 'required'],
            [['stok_sparepart', 'harga_sparepart'], 'integer'],
            [['nama_sparepart', 'jenis_sparepart'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_databarang' => 'Id Databarang',
            'nama_sparepart' => 'Nama Sparepart',
            'jenis_sparepart' => 'Jenis Sparepart',
            'stok_sparepart' => 'Stok Sparepart',
            'harga_sparepart' => 'Harga Sparepart',
        ];
    }
}
