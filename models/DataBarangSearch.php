<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataBarang;

/**
 * DataBarangSearch represents the model behind the search form about `app\models\DataBarang`.
 */
class DataBarangSearch extends DataBarang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_databarang', 'stok_sparepart', 'harga_sparepart'], 'integer'],
            [['nama_sparepart', 'jenis_sparepart'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataBarang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_databarang' => $this->id_databarang,
            'stok_sparepart' => $this->stok_sparepart,
            'harga_sparepart' => $this->harga_sparepart,
        ]);

        $query->andFilterWhere(['like', 'nama_sparepart', $this->nama_sparepart])
            ->andFilterWhere(['like', 'jenis_sparepart', $this->jenis_sparepart]);

        return $dataProvider;
    }
}
