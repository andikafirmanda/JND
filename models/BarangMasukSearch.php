<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BarangMasuk;

/**
 * BarangMasukSearch represents the model behind the search form about `app\models\BarangMasuk`.
 */
class BarangMasukSearch extends BarangMasuk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_barangmasuk', 'id_supplier', 'id_databarang', 'stok_masuk'], 'integer'],
            [['tanggal_masuk'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BarangMasuk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_barangmasuk' => $this->id_barangmasuk,
            'id_supplier' => $this->id_supplier,
            'id_databarang' => $this->id_databarang,
            'tanggal_masuk' => $this->tanggal_masuk,
            'stok_masuk' => $this->stok_masuk,
        ]);

        return $dataProvider;
    }
}
