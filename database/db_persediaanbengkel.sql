-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 01:56 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_persediaanbengkel`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_barangkeluar` int(11) NOT NULL,
  `id_databarang` int(11) NOT NULL,
  `tanggal_keluar` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stok_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barangkeluar`, `id_databarang`, `tanggal_keluar`, `stok_keluar`) VALUES
(1, 2, '2017-01-03 16:53:16', 3),
(2, 1, '2017-01-04 14:07:33', 5),
(3, 1, '2017-01-10 09:03:37', 40),
(4, 3, '2017-01-10 10:53:55', 20),
(5, 3, '2017-01-24 00:12:27', 20),
(6, 3, '2017-01-24 00:12:46', 63),
(7, 1, '2017-01-24 16:32:17', 2),
(8, 2, '2017-01-24 16:50:54', 20),
(11, 3, '2017-01-24 19:17:59', 1),
(12, 1, '2017-01-24 20:00:54', 2),
(13, 1, '2017-01-24 20:01:14', 3);

--
-- Triggers `barang_keluar`
--
DELIMITER $$
CREATE TRIGGER `TG_STOK_BARANGKELUAR` AFTER INSERT ON `barang_keluar` FOR EACH ROW BEGIN 
	UPDATE data_barang SET stok_sparepart=stok_sparepart-NEW.stok_keluar
	WHERE id_databarang=NEW.id_databarang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_barangmasuk` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_databarang` int(11) NOT NULL,
  `tanggal_masuk` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stok_masuk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id_barangmasuk`, `id_supplier`, `id_databarang`, `tanggal_masuk`, `stok_masuk`) VALUES
(1, 1, 1, '2016-12-14 08:11:11', 4),
(2, 1, 1, '2016-12-14 08:11:11', 6),
(3, 1, 2, '2016-12-24 09:27:04', 2),
(5, 1, 1, '2017-01-10 09:12:08', 20),
(6, 1, 1, '2017-01-10 10:44:26', 4),
(7, 1, 3, '2017-01-24 00:10:54', 100),
(8, 1, 2, '2017-01-24 19:21:36', 14);

--
-- Triggers `barang_masuk`
--
DELIMITER $$
CREATE TRIGGER `TG_STOK_BARANGMASUK` AFTER INSERT ON `barang_masuk` FOR EACH ROW BEGIN 
	UPDATE data_barang SET stok_sparepart=stok_sparepart+NEW.stok_masuk
	WHERE id_databarang=NEW.id_databarang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `data_barang`
--

CREATE TABLE `data_barang` (
  `id_databarang` int(11) NOT NULL,
  `nama_sparepart` varchar(50) NOT NULL,
  `jenis_sparepart` varchar(50) NOT NULL,
  `stok_sparepart` int(11) NOT NULL,
  `harga_sparepart` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_barang`
--

INSERT INTO `data_barang` (`id_databarang`, `nama_sparepart`, `jenis_sparepart`, `stok_sparepart`, `harga_sparepart`) VALUES
(1, 'Knalpot  R-9', 'Knalpot', 5, 1000000),
(2, 'RS Racing', 'Knalpot', 11, 60000),
(3, 'Honda', 'Knalpot', 99, 10000000);

-- --------------------------------------------------------

--
-- Table structure for table `data_supplier`
--

CREATE TABLE `data_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(50) NOT NULL,
  `alamat_supplier` varchar(50) NOT NULL,
  `kontak_supplier` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_supplier`
--

INSERT INTO `data_supplier` (`id_supplier`, `nama_supplier`, `alamat_supplier`, `kontak_supplier`) VALUES
(1, 'Andika Firmanda', 'Bandung', '098349890');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barangkeluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_barangmasuk`);

--
-- Indexes for table `data_barang`
--
ALTER TABLE `data_barang`
  ADD PRIMARY KEY (`id_databarang`);

--
-- Indexes for table `data_supplier`
--
ALTER TABLE `data_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_barangkeluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id_barangmasuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `data_barang`
--
ALTER TABLE `data_barang`
  MODIFY `id_databarang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_supplier`
--
ALTER TABLE `data_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
